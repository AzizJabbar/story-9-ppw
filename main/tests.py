from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from .views import home, log_in, log_out, signup
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

class Test(TestCase):
    def test_index_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_home_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_index_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'main/home.html')

    def test_login_url_is_exist(self):
        response = Client().get('/login')
        self.assertEqual(response.status_code, 200)

    def test_login_index_func(self):
        found = resolve('/login')
        self.assertEqual(found.func, log_in)

    def test_login_using_template(self):
        response = Client().get('/login')
        self.assertTemplateUsed(response, 'main/login.html')

    def test_signup_url_is_exist(self):
        response = Client().get('/signup')
        self.assertEqual(response.status_code, 200)

    def test_signup_index_func(self):
        found = resolve('/signup')
        self.assertEqual(found.func, signup)

    def test_signup_using_template(self):
        response = Client().get('/signup')
        self.assertTemplateUsed(response, 'main/signup.html')

    def test_create_user(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        user.save()
        self.assertEqual(User.objects.all().count(), 1)

    def test_login_user_success(self):
        user = User.objects.create(username='testuser')
        user.set_password('testpassword')
        user.save()
        logged_in = Client().login(username='testuser', password='testpassword')
        self.assertTrue(logged_in)

    def test_login_user_fail(self):
        user = User.objects.create(username='testuser')
        user.set_password('testpassword')
        user.save()
        logged_in = Client().login(username='testuser', password='wrongpassword')
        self.assertFalse(logged_in)


    

    
